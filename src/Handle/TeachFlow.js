var /**
	 * 仿真流程管理
	 */
TeachFlow = cc.Class.extend({
	step:0,
	flow:null,
	main:null,
	over_flag: false,
	curSprite:null,
	ctor: function(){
		this.init();
	},
	setMain:function(main){
		this.main=main;
	},
	init: function(){
		for(var i in teach_flow){
			teach_flow[i].finish = false;
			teach_flow[i].cur = false;
			if(teach_flow[i].action == null){
				teach_flow[i].action = ACTION_NONE;
			}
		}
	},
	start:/**
			 * 开始流程
			 */
	function(){
		this.over_flag = false;
		this.step = 0;
		
		/* 新标准，开始的时候，执行下一步 */
		this.next();
	},
	over:/**
			 * 流程结束，计算分数，跳转到结束场景，
			 */
	function(){
		this.over_flag = true;
		this.flow = over;
		gg.lastStep = this.step;
		this.main.over();
	},
	checkTag:/**
				 * 检查是否当前步骤
				 * 
				 * @deprecated 使用新Angel类，不再判断是否当前步骤
				 * @param tag
				 * @returns {Boolean}
				 */
	function(tag){
		var cur_flow = teach_flow[this.step - 1];
		if(cur_flow.tag == tag){
			return true;
		} else {
			return false;
		}
	},
	prev:/**
			 * 回退一定步数
			 * 
			 * @deprecated 需结合具体实现，，暂时不再启动
			 * @param count
			 *            步数
			 */
	function(count){
		if(this.curSprite!=null){
			this.curSprite = null;
		}
		if(this.flow!=null){
			this.flow.cur = false;
		}
		this.step = this.step - count;
		this.flow = teach_flow[this.step - 1];
		this.refresh();
		gg.score -= 11;
	},
	next:/**
			 * 执行下一步操作， 定位当前任务
			 */
	function(){
		if(this.over_flag){
			return;
		}
		if(this.curSprite!=null){
			this.curSprite.setEnable(false)
			this.curSprite = null;
		}
		if(this.flow!=null){
			this.flow.cur = false;
			// 标记任务已完成
			this.flow.finish = true;
		}
		this.flow = teach_flow[this.step++];
		if(this.flow.finish){
			// 如果任务已完成，跳过当前步骤
			this.next();
		}
		this.refresh();
	},
	refresh:/**
			 * 刷新当前任务状态，设置闪现，点击等状态
			 */
	function(){
		// 刷新提示
		this.flow.cur = true;
		if(this.flow.tip != null){
			ll.tip.tip.doTip(this.flow.tip);
		}
		if(this.flow.flash != null){
			ll.tip.flash.doFlash(this.flow.flash);
		}
		if(this.step > teach_flow.length - 1){
			this.over();
		}
		this.initCurSprite();
		if(this.curSprite!=null){
			this.location();
			this.curSprite.setEnable(true);
		}
	},
	location:/**
	 * 定位箭头
	 */
		function(){
		var tag = gg.flow.flow.tag;
		if(tag instanceof Array){
			tag=tag[1];
			if(TAG_LIB_MIN < tag[1]){
				if(ll.run.lib.isOpen()){
					ll.tip.arr.pos(this.curSprite);
				}else{
					//ll.tip.arr.setPosition(gg.width-45,455);

					ll.tip.arr.pos(ll.tool.getChildByTag(TAG_BUTTON_LIB));
				}
			}else{
				ll.tip.arr.pos(this.curSprite);
			}
		}
		else {
			ll.tip.arr.pos(this.curSprite);
		}		
	},
	getStep:/**
			 * 获取当前步数
			 * 
			 * @returns {Number}
			 */
	function(){
		return this.step;
	},
	initCurSprite:/**
					 * 遍历获取当前任务的操作对象
					 */
	function(){
		var tag = this.flow.tag;
		if(tag == null || tag == undefined){
			return;
		}
		var root = ll.run;
		var sprite = null;
		if(tag == TAG_BUTTON_LIB){
			sprite = ll.tool.getChildByTag(tag);
		}else if(tag instanceof Array){
			// 数组
			tag=tag[1];
			if(tag instanceof Array){
				for (var i in tag) {
					root = root.getChildByTag(tag[i]);
				}
				sprite = root;
			}else {
				var sprite = root.getChildByTag(tag);
			}
		}
		if(sprite != null){
			this.curSprite = sprite;
			return;
		}
	}
});
// 任务流
teach_flow = [
	{tip:"1.打开物品库",tag:TAG_BUTTON_LIB},
	//电源线的安装
	{tip:"取出电导仪",tag:[TAG_2,[TAG_LIB,TAG_LIB_INSTRUMENT]]},
	{tip:"取出电源线",tag:[TAG_2,[TAG_LIB,TAG_LIB_POWERLINE]]},
	{tip:"点击电源线,来到电源线安装界面",tag:[TAG_2,TAG_POWERLINE],action:ACTION_DO1},
	{tip:"点击电源线,将电源线插入电导仪",tag:[TAG_2,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_POWERLINE_HEAD]]},
	{tip:"将电源线插在电源插座上",tag:[TAG_2,TAG_POWERLINE],action:ACTION_DO2},
	//电极架的安装
	{tip:"取出电极底座",tag:[TAG_3,[TAG_LIB,TAG_LIB_SEAT]]},
	{tip:"取出电极架",tag:[TAG_3,[TAG_LIB,TAG_LIB_HOLDER]]},
	{tip:"将电极架安装在电极底座上",tag:[TAG_3,TAG_HOLDER],action:ACTION_DO1},
	{tip:"将电极架拉开",tag:[TAG_3,TAG_HOLDER],action:ACTION_DO2},
	//电导电极的安装
	{tip:"取出电导电极",tag:[TAG_4,[TAG_LIB,TAG_LIB_ELECTRODE]]},
	{tip:"将电导电极线展开",tag:[TAG_4,TAG_ELECTRODE],action:ACTION_DO1},
	{tip:"将电导电极架在电极架上",tag:[TAG_4,TAG_ELECTRODE],action:ACTION_DO2},
	{tip:"将电导电极插在电导仪的相应插口",tag:[TAG_4,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_ELECTRODE_HEAD]]},
	//电导仪的调整
	{tip:"点击<ON/OFF>键打开电导仪",tag:[TAG_5,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_ONOROFF]]},
	{tip:"点击<UNIT>键选择RES(电阻率)",tag:[TAG_5,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_UNIT]],action:ACTION_DO1},
	{tip:"点击<UNIT>键选择TDS(可溶解固体总量)",tag:[TAG_5,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_UNIT]],action:ACTION_DO2},
	{tip:"点击<UNIT>键选择SAL(盐度)",tag:[TAG_5,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_UNIT]],action:ACTION_DO3},
	{tip:"点击<UNIT>键选择COND(电导率)",tag:[TAG_5,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_UNIT]],action:ACTION_DO4},
	//蒸馏水的准备
	{tip:"取出烧杯1",tag:[TAG_6,[TAG_LIB,TAG_LIB_BEAKER]],action:ACTION_DO1},
	{tip:"取出蒸馏水瓶",tag:[TAG_6,[TAG_LIB,TAG_LIB_KETTLE]],action:ACTION_DO1},
	{tip:"将蒸馏水瓶中的蒸馏水倒入烧杯中",tag:[TAG_6,TAG_KETTLE]},
	{tip:"将烧杯放到电导电极下方",tag:[TAG_6,TAG_BEAKER]},
	{tip:"点击<CAL>键，进入校准模式",tag:[TAG_6,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_CAL]],action:ACTION_DO1},
	{tip:"将电导电极从电极架上取下,并放入烧杯中,晃动电导电极用于清洗电导电极",tag:[TAG_6,TAG_ELECTRODE],action:ACTION_DO3},
	{tip:"将电导电极左右甩动，甩去上面的蒸馏水",tag:[TAG_6,TAG_ELECTRODE],action:ACTION_DO5},
	//校准液校准
	{tip:"取出装有1408us/cm校准液的瓶子",tag:[TAG_7,[TAG_LIB,TAG_LIB_CALIBRATION]]},
	{tip:"取出烧杯2",tag:[TAG_7,[TAG_LIB,TAG_LIB_BEAKER]],action:ACTION_DO2},
	{tip:"打开装有1408us/cm校准液的瓶子的盖子",tag:[TAG_7,[TAG_CALIBRATION_BOTTLE,TAG_LIB1]]},
	{tip:"将1408us/cm校准液倒入烧杯2中",tag:[TAG_7,[TAG_CALIBRATION_BOTTLE,TAG_CALIBRATION]]},
	{tip:"将烧杯2放到电导电极下方",tag:[TAG_7,TAG_BEAKER1]},
	{tip:"将电导电极从电极架上取下,浸入1408us/cm溶液中,搅动电导电极,并静置一段时间,待出现笑脸",tag:[TAG_7,TAG_ELECTRODE],action:ACTION_DO4},
	{tip:"点击<CAL>键",tag:[TAG_7,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_CAL]],action:ACTION_DO2},
	//清洗电导电极
	{tip:"取出烧杯3",tag:[TAG_8,[TAG_LIB,TAG_LIB_BEAKER]],action:ACTION_DO3},
	{tip:"取出蒸馏水瓶",tag:[TAG_8,[TAG_LIB,TAG_LIB_KETTLE]],action:ACTION_DO2},
	{tip:"将蒸馏水倒入烧杯3中,放到电导电极的下方",tag:[TAG_8,TAG_KETTLE1]},
	{tip:"将电导电极从电极架上取下,并放入烧杯中,晃动电导电极用于清洗电导电极",tag:[TAG_8,TAG_ELECTRODE],action:ACTION_DO6},
	{tip:"将电导电极左右甩动，甩去上面的蒸馏水",tag:[TAG_8,TAG_ELECTRODE],action:ACTION_DO5},
	//未知溶液的测定
	{tip:"取出装有待测液的试剂瓶",tag:[TAG_9,[TAG_LIB,TAG_LIB_BOTTLE]]},
	{tip:"取出烧杯4",tag:[TAG_9,[TAG_LIB,TAG_LIB_BEAKER]],action:ACTION_DO4},
	{tip:"打开试剂瓶盖子",tag:[TAG_9,[TAG_BOTTLE_ALL,TAG_LID]]},
	{tip:"将试剂瓶内待测液倒入烧杯4中",tag:[TAG_9,[TAG_BOTTLE_ALL,TAG_BOTTLE]],action:ACTION_DO1},
	{tip:"将烧杯4放在电导电极下方",tag:[TAG_9,TAG_BEAKER3]},
	{tip:"将电导电极从电极架上取下,并放入烧杯中,搅动电导电极,并静置1分钟左右",tag:[TAG_9,TAG_ELECTRODE],action:ACTION_DO7},
	{tip:"点击<ON/OFF>键关闭电导仪",tag:[TAG_10,[TAG_MP_INSTRUMENT,TAG_MAIN,TAG_BUTTON_ONOROFF]],action:ACTION_DO1},
	{tip:"拔掉电源插座",tag:[TAG_10,TAG_POWERLINE],action:ACTION_DO3},
	{tip:"恭喜过关",over:true}
];
over = {tip:"恭喜过关"};



