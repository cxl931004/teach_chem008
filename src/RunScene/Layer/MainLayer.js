var RunMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	instrument:null,
    ctor:function () {
        this._super();
        this.loadTip();
        this.loadRun();
        this.loadTool();
        this.loadFlow();
        tv = new cc.Sprite();
        tv.setColor(cc.color(66,110,75));
        tv.setTextureRect(cc.rect(0,0,560,220));
        tv.setPosition(640,595);
        this.addChild(tv);
        
        var self = this;      
        return true;
    },
    loadTip:function(){
    	ll.tip = new TipLayer(this);
    },
    loadTool:function(){
    	ll.tool = new ToolLayer(this);
    },
    loadRun:function(){
    	ll.run = new RunLayer(this);
    },
    loadFlow:function(){
    	gg.flow.setMain(this);
    	gg.flow.start();
    },
    over: function (){
    	ll.tip.over();
    	this.scheduleOnce(function(){
    		$.runScene(new FinishScene());
    	},2);
    	// 提交成绩
    	net.saveScore();
    },
});
