var RunLayer = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	running:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.outlet=new cc.Sprite("#equitment/outlet.png");
		this.outlet.setPosition(cc.p(gg.width*0.42, gg.height*0.6));
		this.addChild(this.outlet, 10, TAG_OUTLET);
		this.instrument=new Instrument(this,"#equitment/lite.png",TAG_MP_INSTRUMENT);
		this.initAction();
		
	},
	initAction:function(){
		var animFrames=[];
		for(var i=1;i<5;i++){
			var str="holder/holder"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.1)
		this.runningAction=cc.repeat(cc.animate(animation),1);
		this.runningAction.retain();
	},
	//电导仪
	loadInstrument:function(pos){
		var instrument=new Button(this,11,TAG_INSTRUMENT,"#equitment/Instrument.png",this.callback);
		this.loadInLib(instrument,pos,cc.p(gg.c_width, gg.c_height*0.6));
	},
	//电源线
	loadPowerLine:function(pos){
		var powerLine=new Button(this,10,TAG_POWERLINE,"#equitment/powerLine.png",this.callback);
		this.loadInLib(powerLine, pos,cc.p(gg.c_width*0.83,gg.c_height*0.95));
	},
	//电极底座
	loadSeat:function(pos){
		var seat=new Button(this,10,TAG_SEAT,"#equitment/Seat.png",this.callback);
		this.loadInLib(seat, pos,cc.p(gg.c_width*1.35,gg.c_height*0.6));
	},
	//电极架
	loadHolder:function(pos){
		var holder=new Holder(this,10,TAG_HOLDER,this.callback);
		this.loadInLib(holder, pos,cc.p(gg.c_width*1.55,gg.c_height*0.6));
	},
	//电导电极
	loadElectrode:function(pos){
		var electrode=new Electrode(this,10,TAG_ELECTRODE,this.callback);
		this.loadInLib(electrode, pos,cc.p(gg.c_width*1.55,gg.c_height*0.7));
	},
	//烧杯
	loadBeaker:function(pos){
		if(gg.flow.flow.action==ACTION_DO1){
			var beaker=new Beaker(this, 10, TAG_BEAKER, this.callback);
		}else if(gg.flow.flow.action==ACTION_DO2){
			var beaker=new Beaker(this, 10, TAG_BEAKER1, this.callback);
		}else if(gg.flow.flow.action==ACTION_DO3){
			var beaker=new Beaker(this, 10, TAG_BEAKER2, this.callback);
		}else if(gg.flow.flow.action==ACTION_DO4){
			var beaker=new Beaker(this, 10, TAG_BEAKER3, this.callback);
		}
		this.loadInLib(beaker, pos,cc.p(gg.c_width*1.5,gg.c_height*0.7));
	},
	//蒸馏水瓶
	loadKettle:function(pos){
		if(gg.flow.flow.action==ACTION_DO1){
			var kettle=new Kettle(this,10,TAG_KETTLE,this.callback);
		}else if(gg.flow.flow.action==ACTION_DO2){
			var kettle=new Kettle(this,10,TAG_KETTLE1,this.callback);
		}
			this.loadInLib(kettle, pos,cc.p(gg.c_width*1.7,gg.c_height*0.7));
	},
	//校准液
	loadCalibration:function(pos){
		var calibration=new Calibration(this);
		this.addChild(calibration,10);
		this.loadInLib(calibration, pos,cc.p(gg.c_width*1.8,gg.c_height*0.7));
	},
	//待测液瓶
	loadBottle:function(pos){
		var bottle=new Bottle(this);
		this.addChild(bottle,10);
		this.loadInLib(bottle, pos,cc.p(gg.c_width*1.8, gg.c_height*0.7));
		
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	back:function(){
		var bezier=cc.bezierTo(1, [cc.p(gg.c_width*1.5,gg.c_height*0.7),cc.p(gg.c_width*1.36,gg.c_height*0.2),cc.p(gg.c_width*1.24,gg.c_height*0.5)]);
		var beaker=this.getChildByTag(TAG_BEAKER2);
		beaker.runAction(bezier);
	},
	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var func1=cc.callFunc(this.flowNext, this);
		switch(p.getTag()){
		case TAG_POWERLINE:
			if(gg.flow.flow.action == ACTION_DO1){
				this.instrument.show(true);
			}else if(gg.flow.flow.action == ACTION_DO2){
				gg.flow.next();
				p.setSpriteFrame("equitment/powerLine1.png");
				p.setPosition(cc.p(gg.c_width*0.89,gg.c_height*0.98));
			}else if(gg.flow.flow.action==ACTION_DO3){
				gg.flow.next();
				p.setSpriteFrame("equitment/powerLine.png");
				p.setPosition(cc.p(gg.c_width*0.83,gg.c_height*0.95));
			}
			break;
		case TAG_HOLDER:
			var holder=this.getChildByTag(TAG_HOLDER);
			if(gg.flow.flow.action==ACTION_DO1){
				holder.insert();
			}else if(gg.flow.flow.action==ACTION_DO2){
				p.runAction(cc.sequence(this.runningAction,this.callNext));
			}
			break;
		case TAG_ELECTRODE:
			var electrode=this.getChildByTag(TAG_ELECTRODE);
			var action=gg.flow.flow.action;
			if(gg.flow.flow.action == ACTION_DO1){
				gg.flow.next();
				p.setSpriteFrame("equitment/electrode_1.png");
				var instrument=this.getChildByTag(TAG_MP_INSTRUMENT);
				var main=instrument.getChildByTag(TAG_MAIN);
				var electrode_line=main.getChildByTag(TAG_ELECTRODE_HEAD);
				electrode_line.setVisible(true);
			}else if(gg.flow.flow.action == ACTION_DO2){
				electrode.insert();
			}else if(gg.flow.flow.action==ACTION_DO3){
				electrode.stir(action);
			}else if(gg.flow.flow.action==ACTION_DO5){
				electrode.whipping();
			}else if(gg.flow.flow.action==ACTION_DO4){
				electrode.stir1();
			}else if(gg.flow.flow.action==ACTION_DO6){
				electrode.stir(action);
			}else if(gg.flow.flow.action==ACTION_DO7){
				electrode.stir2();
			}
			break;
		case TAG_KETTLE:
			var ber=cc.bezierBy(1, [cc.p(0,30),cc.p(-5,40),cc.p(-15,60)]);
			var rotate = cc.rotateTo(1, -60);
			var rotate1=cc.rotateTo(1,0);
			var seq=cc.sequence(ber,cc.spawn(rotate,cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
				var beaker=this.getChildByTag(TAG_BEAKER);
				beaker.upLine();
				var kettle=this.getChildByTag(TAG_KETTLE);
				kettle.showFlow();
			},this))),cc.delayTime(3),rotate1,cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);
			},this),func1);
			p.runAction(seq);
			break;
		case TAG_KETTLE1:
			var ber=cc.bezierBy(1, [cc.p(0,30),cc.p(-5,40),cc.p(-15,60)]);
			var rotate = cc.rotateTo(1, -60);
			var rotate1=cc.rotateTo(1,0);
			var bezier=cc.bezierTo(1, [cc.p(gg.c_width*1.5,gg.c_height*0.7),cc.p(gg.c_width*1.36,gg.c_height*0.2),cc.p(gg.c_width*1.24,gg.c_height*0.5)]);
			var beaker=this.getChildByTag(TAG_BEAKER2);
			var kettle=this.getChildByTag(TAG_KETTLE1);
			var seq=cc.sequence(ber,cc.spawn(rotate,cc.sequence(cc.delayTime(0.5),cc.callFunc(function() {
				beaker.upLine();
				kettle.showFlow();
			},this))),cc.delayTime(3),rotate1,cc.fadeOut(0.5),cc.callFunc(function() {
				this.back();
			}, this),cc.callFunc(function(){
				p.removeFromParent(true);
			},this),func1);
			p.runAction(seq);
			break;
		case TAG_BEAKER:
			var beaker=this.getChildByTag(TAG_BEAKER);
			if(gg.flow.flow.action==ACTION_DO1){
				beaker.move1();
			}else{
				beaker.move();
			}
			break;
		case TAG_BEAKER1:
			var beaker=this.getChildByTag(TAG_BEAKER1);
			beaker.move();
			break;
		case TAG_BEAKER2:
			var beaker=this.getChildByTag(TAG_BEAKER2);
			beaker.move();
			break;
		case TAG_BEAKER3:
			var beaker=this.getChildByTag(TAG_BEAKER3);
			beaker.runAction(cc.sequence(cc.callFunc(function() {
				beaker.move();
			},this)));
			break;
		case TAG_INSTRUMENT:
			this.scene.instrument.show();
			p.runAction(func1);
			break;
		default:
		break;
		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_CALIBRATION:
			var beaker=this.getChildByTag(TAG_BEAKER1);
			beaker.upLine();
			break;
			default:
				break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
	}
});