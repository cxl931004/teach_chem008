Kettle = Button.extend({
	ctor:function(parent, zIndex, tag, callback, back){
		this._super(parent, zIndex, tag, "#equitment/kettle.png", callback, back);
		this.initKettle();
		this.setTag(tag);
	},
	initKettle:function(){
		var flow = new cc.Sprite("#equitment/flow_right.png");
		flow.setAnchorPoint(cc.p(0.5,1));
		flow.setRotation(48);
		flow.setOpacity(0);
		flow.setPosition(cc.p(-3,this.height+5));
		this.addChild(flow, 5, TAG_KETTLE_FLOW);
	},
	showFlow:function(){
		var fadein=cc.fadeIn(1)
		var fadeout=cc.fadeOut(1);
		var rotate=cc.rotateBy(2,5);
		var flow=this.getChildByTag(TAG_KETTLE_FLOW);
		flow.runAction(cc.sequence(fadein,rotate,fadeout));
	}
});