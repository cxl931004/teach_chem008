/**
 * 药品库
 */
Lib = cc.Sprite.extend({
	libArr: [],
	openFlag:false,
	doing:false,
	rm : 10,
	ctor:function(p){
		this._super("#lib_back.png");
		p.addChild(this,10);
		this.setTag(TAG_LIB);
		this.init();
	},
	init:function(){
		this.libArr = [];
		this.openFlag = false;
		this.setPosition(gg.width + this.width * 0.5,550);
		
		var fontDef = new cc.FontDefinition();
		fontDef.fontName = "Arial";
		fontDef.fontSize = "48";
		fontDef.fillStyle=cc.color(0, 0, 0,255);
		
		var Instrument=new LibButton(this,10,TAG_LIB_INSTRUMENT,"#equitment/Instrument.png",this.callback);
		Instrument.setPosition(60,this.height*0.5);
		
		var powerLine=new LibButton(this,10,TAG_LIB_POWERLINE,"#equitment/powerLine.png",this.callback);
		powerLine.right(Instrument,this.rm);
		
		var seat=new LibButton(this,10,TAG_LIB_SEAT,"#equitment/Seat.png",this.callback);
		seat.right(powerLine,this.rm);
		
		var holder=new LibButton(this,10,TAG_LIB_HOLDER,"#holder/holder1.png",this.callback);
		holder.right(seat,this.rm)
		
		var electrode=new LibButton(this,10,TAG_LIB_ELECTRODE,"#equitment/electrode.png",this.callback);
		electrode.right(holder,this.rm);
		
		var beaker=new LibButton(this,10,TAG_LIB_BEAKER,"#beaker/beaker.png",this.callback);
		beaker.right(electrode,this.rm);
		
		var kettle=new LibButton(this,10,TAG_LIB_KETTLE,"#equitment/kettle.png",this.callback);
		kettle.right(beaker,this.rm);
		
		var Calibration=new LibButton(this,10,TAG_LIB_CALIBRATION,"#equitment/Calibration.png",this.callback);
		Calibration.right(kettle,this.rm);
		
		var bottle=new LibButton(this,10,TAG_LIB_BOTTLE,"#equitment/bottle2.png",this.callback);
		bottle.right(Calibration,this.rm);
	},
	moveLib:function(tag,width){
		width = 75;
		var begin = false;
		for(var i in this.libArr){
			var libTag = this.libArr[i];
			if(tag == libTag){
				begin = true;
			}
			if(begin){
				var lib = this.getChildByTag(libTag);
				if(lib != null){
					lib.runAction(cc.moveBy(0.5,cc.p(-width, 0)));
				}
			}
		}
	},
	callback:function(p){
		var pos = this.getPosition(); 
		var action = gg.flow.flow.action;
			switch(p.getTag()){
			case TAG_LIB_INSTRUMENT:
				ll.run.loadInstrument(pos);
				break;
			case TAG_LIB_POWERLINE:
				ll.run.loadPowerLine(pos);
				break;
			case TAG_LIB_SEAT:
				ll.run.loadSeat(pos);
				break;
			case TAG_LIB_HOLDER:
				ll.run.loadHolder(pos);
				break;
			case TAG_LIB_ELECTRODE:
				ll.run.loadElectrode(pos);
				break;
			case TAG_LIB_BEAKER:
				ll.run.loadBeaker(pos);
				break;
			case TAG_LIB_KETTLE:
				ll.run.loadKettle(pos);
				break;
			case TAG_LIB_CALIBRATION:
				ll.run.loadCalibration(pos);
				break;
			case TAG_LIB_BOTTLE:
				ll.run.loadBottle(pos);
				break;
			default:
				break;
			}
		if(action == ACTION_NONE){
			this.moveLib(p.getTag(), p.width * p.getScale());
			p.removeFromParent(true);
		}
	},
	isOpen:function(){
		return this.openFlag; 
	},
	open:function(){
		if(this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(-this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = true;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			 if(tag instanceof Array){
				 tag=tag[1];
				 if(TAG_LIB_MIN < tag[1]){
					 // 显示箭头
					 gg.flow.location();
				 }
			 }
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	},
	close:function(){
		if(!this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = false;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				tag=tag[1];
				if(TAG_LIB_MIN < tag[1]){
					// 隐藏箭头
					ll.tip.arr.pos(ll.tool.getChildByTag(TAG_BUTTON_LIB));	
				}
			}
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	}
});
TAG_LIB_MIN = 30019;
TAG_LIB_INSTRUMENT=30020
TAG_LIB_POWERLINE=30021;
TAG_LIB_SEAT=30022;
TAG_LIB_HOLDER=30023;
TAG_LIB_ELECTRODE=30024;
TAG_LIB_BEAKER=30025;
TAG_LIB_KETTLE=30026;
TAG_LIB_CALIBRATION=30027;
TAG_LIB_BOTTLE=30028;
libRelArr = [
             {tag:TAG_LIB_MIN, name:""},
             {tag:TAG_LIB_INSTRUMENT, name:"电导仪"},
             {tag:TAG_LIB_POWERLINE, name:"电源线"},
             {tag:TAG_LIB_SEAT, name:"底座"},
             {tag:TAG_LIB_HOLDER, name:"支架"},
             {tag:TAG_LIB_ELECTRODE, name:"电导电极"},
             {tag:TAG_LIB_BEAKER, name:"烧杯"},
             {tag:TAG_LIB_KETTLE, name:"蒸馏水瓶"},
             {tag:TAG_LIB_CALIBRATION, name:"校准液"},
             {tag:TAG_LIB_BOTTLE, name:"待测液"}];
