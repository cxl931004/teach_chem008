Beaker = Button.extend({
	ctor:function(parent, zIndex, tag, callback, back){
		this._super(parent, zIndex, tag, "#beaker/beaker.png", callback, back);
		this.initBeaker();
		this.setTag(tag);
	},
	initBeaker:function(){
		var line = new cc.Sprite("#beaker/beaker_line.png");
		line.setOpacity(0);
		line.setPosition(this.width * 0.5, line.height * 0.5 + 5);
		this.addChild(line, 5, TAG_BEAKER_LINE);
		
		var label=new cc.Sprite("#equitment/label.png");
		label.setPosition(this.width*0.5,this.height*0.5);
		this.addChild(label, 5, TAG_BEAKER_LABEL);
		
		if(this.getTag()==TAG_BEAKER){
			var title=new cc.Sprite("#equitment/title1.png");
			title.setPosition(this.width*0.5,this.height*0.5);
			this.addChild(title,5);
		}else if(this.getTag()==TAG_BEAKER1){
			var title=new cc.Sprite("#equitment/title2.png");
			title.setPosition(this.width*0.5,this.height*0.5);
			this.addChild(title,5);
		}else if(this.getTag()==TAG_BEAKER2){
			var title=new cc.Sprite("#equitment/title3.png");
			title.setPosition(this.width*0.5,this.height*0.5);
			this.addChild(title,5);
		}else{
			var title=new cc.Sprite("#equitment/title4.png");
			title.setPosition(this.width*0.5,this.height*0.5);
			this.addChild(title,5);
		}
	},
	upLine:function(){
		var line=this.getChildByTag(TAG_BEAKER_LINE);
		var show=cc.fadeTo(0.5,255);
		var moveTo=cc.moveTo(1.5,cc.p(this.width*0.5,this.height*0.68));
		line.runAction(cc.sequence(show,moveTo));
	},
	move:function(){
		var bezier=cc.bezierTo(1, [cc.p(gg.c_width*1.5,gg.c_height*0.7),cc.p(gg.c_width*1.36,gg.c_height*0.2),cc.p(gg.c_width*1.24,gg.c_height*0.5)]);
		this.runAction(cc.sequence(bezier,cc.callFunc(function(){
			gg.flow.next();
		},this)));
	},
	move1:function(){
		var bezier=cc.bezierTo(1, [cc.p(gg.c_width*1.5,gg.c_height*0.7),cc.p(gg.c_width*1.36,gg.c_height*0.2),cc.p(gg.c_width*1.24,gg.c_height*0.46)]);
		this.runAction(cc.sequence(bezier,cc.callFunc(function(){
			gg.flow.next();
		},this)));
	}
});