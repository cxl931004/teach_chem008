Bottle = Widget.extend({
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_BOTTLE_ALL);
	},
	init:function(){
		this.body=new Button(this,10,TAG_BOTTLE,"#equitment/bottle2.png",this.callback);
		var bottle_lib=new Button(this,11,TAG_LID,"#equitment/lid.png",this.callback);
		bottle_lib.setPosition(cc.p(5, this.body.height*0.45));
		
		var flow=new Button(this.body,10,TAG_KETTLE_FLOW1,"#equitment/flow_right.png",this.callback);
		flow.setAnchorPoint(cc.p(0.5,1));
		flow.setScale(1.5);
		flow.setRotation(75);
		flow.setOpacity(0);
		flow.setPosition(cc.p(35,this.body.height+5));
	},
	openLib:function(){
		var ber = cc.bezierBy(1.5,[cc.p(50,70),cc.p(60,-70),cc.p(60,-75)]);
		var rotate = cc.rotateTo(1.5, 180);
		var spawn = cc.spawn(ber, rotate);
		var seq = cc.sequence(spawn,cc.callFunc(function() {
			this.getChildByTag(TAG_LID).setRotation(0);
			this.getChildByTag(TAG_LID).setSpriteFrame("equitment/lid2.png");
			gg.flow.next();
		},this));
		this.getChildByTag(TAG_LID).runAction(seq);
	},
	showFlow:function(){
		var fadein=cc.fadeIn(0.5)
		var fadeout=cc.fadeOut(0.5);
		var rotate=cc.rotateBy(1,10);
		var body=this.getChildByTag(TAG_BOTTLE)
		var flow=body.getChildByTag(TAG_KETTLE_FLOW1);
		flow.runAction(cc.sequence(cc.spawn(fadein,rotate),fadeout));
	},
	callback:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_LID:
			this.openLib();
			break;
		case TAG_BOTTLE:
			this.body.runAction(cc.sequence(cc.moveBy(1,cc.p(-gg.c_width*0.12,80)),cc.spawn(cc.rotateTo(1,-85),
					cc.sequence(cc.delayTime(0.5),func)),cc.delayTime(1),cc.rotateTo(1,0),cc.callFunc(function() {
				var Bottle=ll.run.getChildByTag(TAG_BOTTLE_ALL);
				Bottle.setVisible(false);
				p.removeFromParent(true);
				gg.flow.next();
			},this)));
			break;
		default:
			break;
		}
	},
	actionDone:function(p){
		switch(p.getTag()){
		case TAG_BOTTLE:
			var beaker=ll.run.getChildByTag(TAG_BEAKER3);
			beaker.upLine();
			this.showFlow();
			break;
		default:
			break;
		}
	}
});
