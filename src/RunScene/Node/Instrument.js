var Instrument=Win.extend({
		p:null,
		name:null,
		ctor:function (parent,name,tag) {
			this._super(parent,name);
			this.p=parent;
			this.name=name;
			this.setTag(tag);
			this.initSp();
		},
		initSp:function (){
			//电导仪
			var instrument=new Button(this.main,10,TAG_INSTRUMENT,"#equitment/instrument1.png");
			instrument.setPosition(cc.p(this.main.width*0.5,this.main.height*0.5));
			instrument.setVisible(false);
			//电导仪背面
			var instrument1=new Button(this.main,10,TAG_INSTRUMENT_BACK,"#equitment/Instrument_back.png");
			instrument1.setPosition(cc.p(this.main.width*0.5,this.main.height*0.5));
			//插孔
			var socket=new Button(this.main,10,TAG_SOCKET,"#equitment/socket.png");
			socket.setPosition(cc.p(this.main.width*0.65, this.main.height*0.445));
			//遮挡
			var instrument_back2=new Button(this.main,11,TAG_INSTRUMENT_BACK2,"#equitment/Instrument_back2.png");
			instrument_back2.setPosition(cc.p(this.main.width*0.496, this.main.height*0.5));
			
			var instrument_back1=new Button(this.main,11,TAG_INSTRUMENT_BACK1,"#equitment/Instrument_back1.png");
			instrument_back1.setPosition(cc.p(this.main.width*0.492, this.main.height*0.5));
			
			var powerline=new Button(this.main,10,TAG_POWERLINE_HEAD,"#equitment/line1.png",this.callback);
			powerline.setPosition(cc.p(this.main.width*0.64, this.main.height*0.3));
			
			var electrode_line=new Button(this.main,10,TAG_ELECTRODE_HEAD,"#equitment/line2.png",this.callback);
			electrode_line.setPosition(cc.p(this.main.width*0.52,this.main.height*0.3));
			electrode_line.setVisible(false);
			//按钮
			var onOroff=new Button(this.main,11,TAG_BUTTON_ONOROFF,"#equitment_button/onOroff.png",this.callback);
			onOroff.setPosition(cc.p(this.main.width*0.502,this.main.height*0.473));
			onOroff.setVisible(false);
			
			var unit=new Button(this.main,11,TAG_BUTTON_UNIT,"#equitment_button/Unit.png",this.callback);
			unit.setPosition(cc.p(this.main.width*0.623,this.main.height*0.322));
			unit.setVisible(false);
			
			var cal=new Button(this.main,11,TAG_BUTTON_CAL,"#equitment_button/CAL.png",this.callback);
			cal.setPosition(cc.p(this.main.width*0.375, this.main.height*0.478));
			cal.setVisible(false);
			//cal
			var cal_tag=new Button(this.main,11,TAG_CAL,"#tag/CAL.png");
			cal_tag.setPosition(cc.p(this.main.width*0.63, this.main.height*0.807));
			cal_tag.setVisible(false);
			//cond
			var cond=new Button(this.main,11,TAG_COND,"#tag/COND.png");
			cond.setPosition(cc.p(this.main.width*0.5,this.main.height*0.808));
			cond.setVisible(false);
			//time
			var time=new Button(this.main,11,TAG_TIME,"#tag/time.png");
			time.setPosition(cc.p(this.main.width*0.64, this.main.height*0.805));
			time.setVisible(false);
			//数据
			var num=new Button(this.main,11,TAG_NUM,"#tag/num1.png");
			num.setPosition(cc.p(this.main.width*0.515, this.main.height*0.732));
			num.setVisible(false);
			//笑脸
			var smile=new Button(this.main,11,TAG_SMILE,"#tag/smile.png");
			smile.setPosition(cc.p(this.main.width*0.38, this.main.height*0.68));
			smile.setVisible(false);
			//温度
			var Temperature=new Button(this.main,11,TAG_TEMPERATURE,"#tag/25.0.png");
			Temperature.setPosition(cc.p(this.main.width*0.565, this.main.height*0.68));
			Temperature.setVisible(false);
			//单位
			var us=new Button(this.main,11,TAG_US,"#tag/us.png");
			us.setPosition(cc.p(this.main.width*0.64, this.main.height*0.703));
			us.setVisible(false);
			//mtc
			var mtc=new Button(this.main,11,TAG_MTC,"#tag/MTC.png");
			mtc.setPosition(cc.p(this.main.width*0.64, this.main.height*0.68));
			mtc.setVisible(false);
			//atc
			var atc=new Button(this.main,11,TAG_ATC,"#tag/ATC.png");
			atc.setPosition(cc.p(this.main.width*0.64, this.main.height*0.68));
			atc.setVisible(false);
			//M
			var M=new Button(this.main,11,TAG_M,"#tag/M.png");
			M.setPosition(cc.p(this.main.width*0.38, this.main.height*0.64));
			M.setVisible(false);
			//1M
			var M1=new Button(this.main,11,TAG_M1,"#tag/1m+.png");
			M1.setPosition(cc.p(this.main.width*0.635, this.main.height*0.64));
			M1.setVisible(false);
			//RES
			var res=new Button(this.main,11,TAG_RES,"#tag/RES.png");
			res.setPosition(cc.p(this.main.width*0.465,this.main.height*0.8));
			res.setVisible(false);
			//TDS
			var tds=new Button(this.main,11,TAG_TDS,"#tag/TDS.png");
			tds.setPosition(cc.p(this.main.width*0.5,this.main.height*0.8));
			tds.setVisible(false);
			//SAL
			var sal=new Button(this.main,11,TAG_SAL,"#tag/SAL.png");
			sal.setPosition(cc.p(this.main.width*0.535,this.main.height*0.8));
			sal.setVisible(false);
			
			var ppt=new Button(this.main,11,TAG_PPT,"#tag/PPT.png");
			ppt.setPosition(cc.p(this.main.width*0.45, this.main.height*0.68));
			ppt.setVisible(false);
			
			var mc=new Button(this.main,11,TAG_MC,"#tag/mc.png");
			mc.setPosition(cc.p(this.main.width*0.45, this.main.height*0.68));
			mc.setVisible(false);
		},
		callback:function(pSender){
			var action=gg.flow.flow.action;
			var instrument_1=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
			var main=instrument_1.getChildByTag(TAG_MAIN);
			var instrument=main.getChildByTag(TAG_INSTRUMENT);
			var onOroff=main.getChildByTag(TAG_BUTTON_ONOROFF);
			var instrument1=main.getChildByTag(TAG_INSTRUMENT_BACK);
			var socket=main.getChildByTag(TAG_SOCKET);
			var instrument_back2=main.getChildByTag(TAG_INSTRUMENT_BACK2);
			var powerLine=main.getChildByTag(TAG_POWERLINE_HEAD);
			var unit=main.getChildByTag(TAG_BUTTON_UNIT);
			var cal=main.getChildByTag(TAG_BUTTON_CAL);
			var electrode_line=main.getChildByTag(TAG_ELECTRODE_HEAD);
			var instrument_back1=main.getChildByTag(TAG_INSTRUMENT_BACK1);
			var cal_tag=main.getChildByTag(TAG_CAL);
			var cond=main.getChildByTag(TAG_COND);
			var time=main.getChildByTag(TAG_TIME);
			var num=main.getChildByTag(TAG_NUM);
			var smile=main.getChildByTag(TAG_SMILE);
			var temperature=main.getChildByTag(TAG_TEMPERATURE);
			var us=main.getChildByTag(TAG_US);
			var mtc=main.getChildByTag(TAG_MTC);
			var m=main.getChildByTag(TAG_M);
			var m1=main.getChildByTag(TAG_M1);
			var res=main.getChildByTag(TAG_RES);
			var tds=main.getChildByTag(TAG_TDS);
			var sal=main.getChildByTag(TAG_SAL);
			var atc=main.getChildByTag(TAG_ATC);
			var ppt=main.getChildByTag(TAG_PPT);
			var mc=main.getChildByTag(TAG_MC);
			var instrument_1=ll.run.getChildByTag(TAG_INSTRUMENT);
			switch(pSender.getTag()){
			case TAG_BUTTON_ONOROFF:
				gg.flow.next();
				if(action==ACTION_DO1){
					instrument.setSpriteFrame("equitment/instrument1.png");
					cond.setVisible(false);
					num.setVisible(false);
					us.setVisible(false);
					smile.setVisible(false);
					m.setVisible(false);
					m1.setVisible(false);
					instrument_1.setSpriteFrame("equitment/Instrument.png");
				}else{
					instrument.setSpriteFrame("equitment/instrument2.png");
					cond.setVisible(true);
					time.setVisible(true);
					num.setVisible(true);
					us.setVisible(true);
					smile.setVisible(true);
					temperature.setVisible(true);
					mtc.setVisible(true);
					m1.setVisible(true);
					cal.setVisible(true);
					instrument_1.setSpriteFrame("equitment/Instrument_1.png");
					unit.setVisible(true);
				}
				break;
			case TAG_BUTTON_UNIT:
				gg.flow.next();
				if(action==ACTION_DO1){
					cond.setVisible(false);
					res.setVisible(true);
					mc.setVisible(true);
					m.setVisible(false);
					us.setVisible(false);
				}else if(action==ACTION_DO2){
					res.setVisible(false);
					mc.setVisible(false);
					tds.setVisible(true);
				}else if(action==ACTION_DO3){
					tds.setVisible(false);
					sal.setVisible(true);
					ppt.setVisible(true);
				}else if(action==ACTION_DO4){
					ppt.setVisible(false);
					sal.setVisible(false);
					cond.setVisible(true);
					us.setVisible(true);
				}
				break;
			case TAG_POWERLINE_HEAD:
				var move=cc.moveTo(1,cc.p(main.width*0.64, main.height*0.43));	
				var move1=cc.moveTo(1,cc.p(gg.c_width*0.83,gg.c_height*0.9));
				pSender.runAction(cc.sequence(cc.spawn(move,cc.callFunc(function() {
					var powerline=ll.run.getChildByTag(TAG_POWERLINE);
					powerline.runAction(move1);
				}, this)),cc.callFunc(function(){
					gg.flow.next();
				},this)));
				break;
			case TAG_BUTTON_CAL:
				if(action==ACTION_DO2){
					smile.setVisible(false);
					temperature.setVisible(false);
					mtc.setVisible(false);
					var beaker=ll.run.getChildByTag(TAG_BEAKER1);
					beaker.setVisible(false);
					cal_tag.setVisible(false);
					pSender.runAction(cc.sequence(cc.callFunc(function() {
						num.setSpriteFrame("tag/num7.png");
						num.runAction(cc.sequence(cc.fadeIn(0.5),cc.fadeOut(0.5)).repeat(3));
					},this),cc.delayTime(3),cc.callFunc(function() {
						num.runAction(cc.fadeIn(0.5));;
						num.setSpriteFrame("tag/END.png");
						us.setVisible(false);
					},this),cc.delayTime(2),cc.callFunc(function(){
						num.setSpriteFrame("tag/num7.png");
						us.setVisible(true);
						m.setVisible(true);
					}, this),cc.callFunc(function() {
						gg.flow.next();
					}, this)));
				}else if(action==ACTION_DO1){
					gg.flow.next();
					temperature.setSpriteFrame("tag/23.3.png");
					time.setVisible(false);
					var fadeout=cc.fadeOut(0.5);
					var fadein=cc.fadeIn(0.5)
					cal_tag.setVisible(true);
					cal_tag.runAction(cc.repeatForever(cc.sequence(fadeout,fadein)));
				}
				break;
			case TAG_ELECTRODE_HEAD:
				var move=cc.moveTo(1,cc.p(main.width*0.51, main.height*0.39));	
				var move1=cc.moveTo(1,cc.p(gg.c_width*0.83,gg.c_height*0.9));
				pSender.runAction(cc.sequence(move,cc.callFunc(function() {
					electrode_line.setVisible(false);
					powerLine.setVisible(false);
					instrument_back1.setVisible(false);
					instrument_back2.setVisible(false);
					instrument1.setVisible(false);
					instrument.setVisible(true);
					socket.setVisible(false);
					onOroff.setVisible(true);
				}, this),cc.callFunc(function(){
					gg.flow.next();
				},this)));
				break;
			default:
				break;
			}
		}
});