Win = cc.Node.extend({
	main:null,
	close:null,
	ctor:function(p, name){
		this._super();
		p.addChild(this, 30);
		this.init(name);
	},
	init:function(name){
		this.setCascadeOpacityEnabled(true);
		this.setScale(0);
		this.main = new cc.Sprite(name);
		this.addChild(this.main,1,TAG_MAIN);
		
//		this.close = new Button(this, 7, TAG_WIN_CLOSE, "#button/win_close.png",this.close);
		var mp = this.main.getAnchorPoint();
//		var cp = this.close.getAnchorPoint();
//		this.close.setPosition(this.main.width * (1 - mp.x) - this.close.width * (1 - cp.x) - 10, 
//				this.main.height * (1 - mp.y) - this.close.height * (1 - cp.y) - 5);
	},
	show:function(next){
		var move = cc.moveTo(0.5, gg.width * 0.18, gg.height * 0.5);
		var scale = cc.scaleTo(0.5, 1);
		var spawn = cc.spawn(move, scale);
		var seq = cc.sequence(spawn, cc.callFunc(function(){
			if(next){
				gg.flow.next();
			}
		},this));
		this.runAction(seq);
	},
	close:function(p){
		var seq = cc.sequence(cc.fadeTo(0.5,0),cc.callFunc(function(){
			if(gg.flow.flow.action == ACTION_DO1){
				this.getParent().loadPaper();
			} else if(gg.flow.flow.action == ACTION_DO2){
				gg.flow.next();
			}
			this.removeFromParent(true);
		},this));
		this.runAction(seq);
	}
});