Calibration = Widget.extend({
	ctor:function(){
		this._super();
		this.init();
		this.setTag(TAG_CALIBRATION_BOTTLE);
	},
	init:function(){
		this.body=new Button(this,10,TAG_CALIBRATION,"#equitment/Calibration.png",this.callback,this);
		var calibration_lib=new Button(this,11,TAG_LIB1,"#equitment/Calibration_lib.png",this.callback,this);
		calibration_lib.setPosition(cc.p(0, this.body.height*0.5));
		
		var flow=new Button(this.body,9,TAG_FLOW,"#equitment/flow_right.png",this.callback,this);
		flow.setAnchorPoint(cc.p(1,1));
		flow.setScale(1.5,1);
		flow.setRotation(40);
		flow.setOpacity(0);
		flow.setPosition(cc.p(this.body.width*0.1,this.body.height*0.985));
	},
	openLib:function(){
		var ber = cc.bezierBy(1.5,[cc.p(50,70),cc.p(60,-70),cc.p(60,-75)]);
		var rotate = cc.rotateTo(1.5, 180);
		var spawn = cc.spawn(ber, rotate);
		var seq = cc.sequence(spawn,cc.callFunc(function() {
			this.getChildByTag(TAG_LIB1).setRotation(0);
			this.getChildByTag(TAG_LIB1).setSpriteFrame("equitment/Calibration_lib1.png");
			gg.flow.next();
		},this));
		this.getChildByTag(TAG_LIB1).runAction(seq);
	},
	showLine:function(){
		var fadein=cc.fadeIn(0.5);
		var fadeout=cc.fadeOut(0.5);
		var body=this.getChildByTag(TAG_CALIBRATION);
		var flow=body.getChildByTag(TAG_FLOW);
		var rotate=cc.rotateBy(1,35);
		flow.runAction(cc.sequence(fadein,rotate,fadeout));
	},
	callback:function(p){
		var beaker=ll.run.getChildByTag(TAG_BEAKER1);
		var calibration_lib=this.getChildByTag(TAG_LIB1);
		var func=cc.callFunc(function() {
			gg.flow.next();
		},this);
		switch(p.getTag()){
		case TAG_LIB1:
			this.openLib();
			break;
		case TAG_CALIBRATION:
			var move=cc.moveBy(1,cc.p(-gg.c_width*0.205,80));
			var rotate=cc.rotateBy(3,-85);
			p.runAction(cc.sequence(move,cc.spawn(rotate,cc.sequence(cc.delayTime(1.5),cc.callFunc(function() {
				this.showLine();
				beaker.upLine();
			},this))),cc.rotateBy(1,0),cc.callFunc(function() {
				p.setVisible(false);
				calibration_lib.setVisible(false);
			},this),func));
			break;
		default:
			break;
		}
	}
//	actionDone:function(p){
//		switch(p.getTag()){
//		case TAG_CALIBRATION:
//			var beaker=ll.run.getChildByTag(TAG_BEAKER1);
//			beaker.upLine();
//			this.showLine();
//			break;
//			default:
//				break;
//	}
//	}
});
