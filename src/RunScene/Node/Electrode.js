Electrode = Button.extend({
	runningAction:null,
	runningAction1:null,
	runningAction2:null,
	ctor:function(parent, zIndex, tag, callback, back){
		this._super(parent, zIndex, tag, "#equitment/electrode.png", callback, back);
		this.setTag(tag);
		this.initAction();
		this.initAction1();
		this.initNumAction();
	},
	initAction:function(){
		var animFrames=[];
		for(var i=1;i<7;i++){
			var str="electrode/electrode"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.2)
		this.runningAction=cc.repeat(cc.animate(animation),3);
		this.runningAction.retain();
	},
	initAction1:function(){
		var animFrames=[];
		for(var i=1;i<5;i++){
			var str="electrode_1/electrode"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.2) 
		this.runningAction1=cc.repeat(cc.animate(animation),3);
		this.runningAction1.retain();
	},
	initNumAction:function(){
		var animFrames=[];
		for(var i=1;i<7;i++){
			var str="tag/num"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.5)
		this.runningAction2=cc.repeat(cc.animate(animation),1);
		this.runningAction2.retain();
	},
	insert:function(){
		var moveto=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*1.5));
		var moveto1=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*0.95));
	 	this.setSpriteFrame("electrode/electrode1.png");
		this.runAction(cc.sequence(moveto,moveto1,cc.callFunc(function(){
			var instrument=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
			var main=instrument.getChildByTag(TAG_MAIN);
			var cal=main.getChildByTag(TAG_BUTTON_CAL);
			this.setSpriteFrame("equitment/electrode_2.png");
			this.setPosition(cc.p(gg.c_width*1.2,gg.c_height*0.85));
		},this),cc.callFunc(function(){
			gg.flow.next();
		},this)));
	},
	//水的搅拌
	stir:function(action){
		var instrument=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
		var main=instrument.getChildByTag(TAG_MAIN);				
		var num=main.getChildByTag(TAG_NUM);
		var smile=main.getChildByTag(TAG_SMILE);
		this.setSpriteFrame("electrode/electrode1.png");
		this.setPosition(cc.p(gg.c_width*1.29,gg.c_height*0.95));
		var moveto=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*1.1));
		var moveto1=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*0.8));
		this.runAction(cc.sequence(moveto,moveto1,cc.callFunc(function(){
			smile.setVisible(false);
		},this),cc.spawn(this.runningAction,cc.callFunc(function(){
			num.runAction(this.runningAction2);
		},this)),cc.bezierTo(1, [cc.p(gg.c_width*1.29,gg.c_height*0.95),cc.p(gg.c_width*1.35,gg.c_height),cc.p(gg.c_width*1.5,gg.c_height*1.2)]),cc.callFunc(function(){
			if(action==ACTION_DO6){
				var beaker=ll.run.getChildByTag(TAG_BEAKER2);
			}else{
				var beaker=ll.run.getChildByTag(TAG_BEAKER);
			}
				beaker.setVisible(false);
		},this),cc.callFunc(function(){
			gg.flow.next();
		},this)));
	},
	whipping:function(){
		var instrument=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
		var main=instrument.getChildByTag(TAG_MAIN);				
		var num=main.getChildByTag(TAG_NUM);
		this.runAction(cc.sequence(cc.spawn(this.runningAction1,cc.callFunc(function() {
			num.runAction(this.runningAction2.reverse());
		},this)),cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*0.95)),cc.callFunc(function(){
			this.setSpriteFrame("equitment/electrode_2.png");
			this.setPosition(cc.p(gg.c_width*1.2,gg.c_height*0.85));
		},this),cc.callFunc(function(){
			gg.flow.next();
		},this)));
	},
	//
	stir1:function(){
		var instrument=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
		var main=instrument.getChildByTag(TAG_MAIN);				
		var num=main.getChildByTag(TAG_NUM);
		this.setSpriteFrame("electrode/electrode1.png");
		this.setPosition(cc.p(gg.c_width*1.29,gg.c_height*0.95));
		var moveto=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*1.1));
		var moveto1=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*0.85));
		this.runAction(cc.sequence(moveto,moveto1,cc.spawn(this.runningAction,cc.callFunc(function() {
			num.runAction(this.runningAction2);
		}, this)),cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*0.95)),cc.callFunc(function(){
			this.setSpriteFrame("equitment/electrode_2.png");
			this.setPosition(cc.p(gg.c_width*1.2,gg.c_height*0.85));
		},this),cc.delayTime(0.5),cc.callFunc(function() {
			ll.run.clock.doing();
		}, this),cc.delayTime(2),cc.callFunc(function() {
			ll.run.clock.stop();
			ll.run.clock.time.setVisible(true);
			var instrument=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
			var main=instrument.getChildByTag(TAG_MAIN);
			var smile=main.getChildByTag(TAG_SMILE);
			var num=main.getChildByTag(TAG_NUM);
			num.setSpriteFrame("tag/num6.png");
			smile.setVisible(true);
		}, this),cc.delayTime(1),cc.callFunc(function(){
			ll.run.clock.time.setVisible(false);
			ll.run.clock.setVisible(false);
		},this),cc.callFunc(function(){
			gg.flow.next();
		},this)));
	},
	stir2:function(){
		var beaker=ll.run.getChildByTag(TAG_BEAKER2);
		var instrument=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
		var main=instrument.getChildByTag(TAG_MAIN);				
		var num=main.getChildByTag(TAG_NUM);
		this.setSpriteFrame("electrode/electrode1.png");
		this.setPosition(cc.p(gg.c_width*1.29,gg.c_height*0.95));
		var moveto=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*1.1));
		var moveto1=cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*0.85));
		this.runAction(cc.sequence(moveto,moveto1,cc.spawn(this.runningAction,cc.callFunc(function() {
			num.runAction(this.runningAction2);
		}, this)),cc.moveTo(1,cc.p(gg.c_width*1.29,gg.c_height*0.95)),cc.callFunc(function(){
			this.setSpriteFrame("equitment/electrode_2.png");
			this.setPosition(cc.p(gg.c_width*1.2,gg.c_height*0.85));
		},this),cc.callFunc(function() {
			ll.run.clock.doing();
		}, this),cc.delayTime(2),cc.callFunc(function() {
			ll.run.clock.stop();
			ll.run.clock.time.setVisible(true);
			var instrument=ll.run.getChildByTag(TAG_MP_INSTRUMENT);
			var main=instrument.getChildByTag(TAG_MAIN);
			var smile=main.getChildByTag(TAG_SMILE);
			var num=main.getChildByTag(TAG_NUM);
			num.setSpriteFrame("tag/num6.png");
			smile.setVisible(true);
		}, this),cc.delayTime(1),cc.callFunc(function(){
			ll.run.clock.time.setVisible(false);
			ll.run.clock.setVisible(false);
		},this),cc.callFunc(function(){
			gg.flow.next();
		},this)));
	}
});		